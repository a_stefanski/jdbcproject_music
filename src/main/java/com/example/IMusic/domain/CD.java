package com.example.IMusic.domain;

import java.util.ArrayList;
import java.util.List;

public class CD {
	
	private int id;
	private String name;
	private int yearOfRelease;
	
	public CD() {
	}
	
	public CD(String name, int yearOfRelease) {
		super();
		this.name = name;
		this.yearOfRelease = yearOfRelease;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getYearOfRelease() {
		return yearOfRelease;
	}
	public void setYearOfRelease(int yearOfRelease) {
		this.yearOfRelease = yearOfRelease;
	}	
}
