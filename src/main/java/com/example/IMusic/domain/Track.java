package com.example.IMusic.domain;

public class Track
{
	private int id;
	private String trackName;
	private int trackLengthInSeconds;

	private int sizeOfTracks = 0;
	private int sizeOfTracksWithCD = 0;

	public Track(){}

	public Track(String trackName, int trackLengthInSeconds)
	{
		super();
		this.trackName = trackName;
		this.trackLengthInSeconds = trackLengthInSeconds;
	}

	public int getTrackID(){return id;}
	public void setTrackID(int id){this.id = id;}

	public String getTrackName(){return trackName;}
	public void setTrackName(String trackName){this.trackName = trackName;}

	public int getTrackLengthInSeconds(){return trackLengthInSeconds;}
	public void setTrackLengthInSeconds(int trackLengthInSeconds){this.trackLengthInSeconds = trackLengthInSeconds;}

	public int getSizeOfTracks(){return sizeOfTracks;}
	public int getSizeOfTracksWithCD(){return sizeOfTracksWithCD;}

	public void setSizeOfTracksWithCD(int sizeOfTracksWithCD){this.sizeOfTracksWithCD = sizeOfTracksWithCD;}
	public void setSizeOfTracks(int sizeOfTracks){this.sizeOfTracks = sizeOfTracks;}



}