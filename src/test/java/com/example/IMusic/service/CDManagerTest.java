package com.example.IMusic.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import com.example.IMusic.domain.CD;
import com.example.IMusic.domain.Track;



public class CDManagerTest {
	
	
	CDManager cdManager = new CDManager();
	
	private final static String NAME_1 = "Ameba";
	private final static int YOR_1 = 1945;
	private final static String TRACK_NAME = "Chaos";
	private final static int TLIS = 214;
	
	@Test
	public void checkConnection(){
		assertNotNull(cdManager.getConnection());
	}
	
	@Test
	public void checkAdding(){
		
		CD cd = new CD(NAME_1, YOR_1);
		
		cdManager.clearCDs();
		assertEquals(1,cdManager.addCD(cd));
		
		List<CD> cds = cdManager.getAllCDs();
		CD cdRetrieved = cds.get(0);
		
		assertEquals(NAME_1, cdRetrieved.getName());
		assertEquals(YOR_1, cdRetrieved.getYearOfRelease());
		
	}

	@Test
	public void checkDeleteCD()
	{
		cdManager.clearCDs();
		cdManager.addSimpleCD();
		cdManager.addSimpleCD();
		
		List<CD> cd = cdManager.getAllCDs();

		int idCD = cd.get(cdManager.getAllCDs().size()-1).getId();

		assertEquals(1, cdManager.deleteCD(idCD));
	}

	@Test
	public void checkUpdateCD()
	{
		cdManager.clearCDs();
		cdManager.addSimpleCD();
		List<CD> cd = cdManager.getAllCDs();

		String tmpName = "Ameba";
		int tmpYor = 2000;
		int idCD = cd.get(cdManager.getAllCDs().size()-1).getId();

		cdManager.updateCD(idCD, tmpName, tmpYor);

		CD cdRetrieved = cdManager.getAllCDs().get(cdManager.getAllCDs().size() - 1);

		assertEquals(tmpName, cdRetrieved.getName());
		assertEquals(tmpYor, cdRetrieved.getYearOfRelease());
	}

	@Test
	public void checkAddTrack()
	{
		Track newTrack = new Track(TRACK_NAME, TLIS);
		
		cdManager.clearTracks();
		assertEquals(1,cdManager.addTrack(newTrack));
		
		List<Track> track = cdManager.getAllTracks();
		Track trackRetrieved = track.get(0);
		
		assertEquals(TRACK_NAME, trackRetrieved.getTrackName());
		assertEquals(TLIS, trackRetrieved.getTrackLengthInSeconds());
	}

	@Test
	public void checkAddingTrackToCD()
	{
		cdManager.clearTracks();
		cdManager.clearCDs();
		cdManager.addSimpleCD();
		cdManager.addSimpleTrack();
		List<CD> cd = cdManager.getAllCDs();
		List<Track> track = cdManager.getAllTracks();

		int idCD = cd.get(cdManager.getAllCDs().size()-1).getId();
		int idTrack = track.get(cdManager.getAllTracks().size()-1).getTrackID();

		cdManager.addTrackToCD(idCD, idTrack);

		CD cdRetrieved = cdManager.getAllCDs().get(cdManager.getAllCDs().size() - 1);

		assertEquals(idCD, cdManager.checkIDAddedToTrack(idTrack));
	}

	@Test
	public void checkDeletingCDFromTrack()
	{
		cdManager.clearTracks();
		cdManager.clearCDs();
		cdManager.addSimpleCD();
		cdManager.addSimpleTrack();
		
		List<CD> cd = cdManager.getAllCDs();
		List<Track> track = cdManager.getAllTracks();

		int idCD = cd.get(cdManager.getAllCDs().size()-1).getId();
		int idTrack = track.get(cdManager.getAllTracks().size()-1).getTrackID();

		cdManager.deleteCDFromTrack(idTrack);

		assertEquals(0, cdManager.checkDeletedCDFromTrack(idTrack));
	}
}
